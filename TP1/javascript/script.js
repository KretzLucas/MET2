$("button").on("click", function()
{
    var inputs = document.getElementsByTagName('input');
    var remplis = true;
    var i = 0;
               
    while(i < inputs.length && remplis)
    {
        if(inputs[i].value === '')
        {
               remplis = false;
        }
        i++;
    }
               
    if(!remplis)
    {
        alert("Veuillez remplir tous les champs !");
    }
    else
    {
        var mdp = $("#password").val();
        var confirm = $("#confirm").val();
               
        if(confirm != mdp)
        {
            alert("Les mots de passe ne sont pas identiques !");
        }
    }
});

$("#civiliteMme").change(function(){

       $("#civiliteMr").prop("checked", false);

});
$("#civiliteMr").change(function(){

       $("#civiliteMme").prop("checked", false);

});
